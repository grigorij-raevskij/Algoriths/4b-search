﻿#include <iostream>
#include <vector>
#include <algorithm>
#include "optimization.h"
#include <string>
#include <sstream>

int main()
{
    int n, k;
    uint64_t x;
    std::vector<uint64_t> arr;
    std::cin >> n >> k;
    std::cin.ignore();
    std::string line;
    std::getline(std::cin, line);
    std::stringstream ss(line);
    while (ss >> x)
        arr.push_back(x);
    std::getline(std::cin, line);
    std::stringstream ss1(line);
    while (ss1 >> x)
        std::cout << (std::binary_search(arr.begin(), arr.end(), x) ? "YES" : "NO") << std::endl;
    return 0;
}